#!/bin/python

#    ShoutBot An IRC Bot that shouts back the contents of all messages from a specific user.
#    Copyright (C) 2014 Liam Middlebrook
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.words.protocols.irc import IRCClient
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.internet import reactor

import os
import yaml

base_dir = os.path.split(__file__)[0]
settings = None

class ShoutBot(IRCClient):
    sourceURL = "https://bitbucket.org/liam-middlebrook/shoutbot"
    lineRate = 1
    versionNum = 1

    def signedOn(self):
        self.join(self.factory.channel)
        self.factory.add_bot(self)
    
    def joined(self, channel):
        print str("Joined " + channel)

    def left(self, channel):
        print str("Left " + channel)

    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]

        if user in settings['target']:
            self.msg(channel, msg.upper())

class ShoutBotFactory(ReconnectingClientFactory):
    active_bot = None

    def __init__(self, protocol=ShoutBot):
        self.protocol = protocol
        self.channel = settings['channel']
        IRCClient.nickname = settings['name']
        IRCClient.realname = settings['name']

    def add_bot(self, bot):
        self.active_bot = bot

if __name__ == '__main__':
    with open(os.path.join(base_dir, 'shoutbot.yaml')) as settings_yaml:
        settings = yaml.load(settings_yaml)

    f = ShoutBotFactory()
    reactor.connectTCP(settings['server'], 6667, f)
    reactor.run()
